
document.addEventListener('DOMContentLoaded', function() {
  
  let  anchor = document.createElement('a');
  anchor.textContent = 'Learn More';
  anchor.setAttribute('href', '#');

  let footer = document.querySelector('footer');
  footer.appendChild(anchor);
});








document.addEventListener('DOMContentLoaded', function() {

  let select = document.createElement('select');
  select.setAttribute('id', 'rating');


  function addOption(value, text) {
  let  option = document.createElement('option');
    option.setAttribute('value', value);
    option.textContent = text;
    select.appendChild(option);
  }

 
  addOption('4', '4 Stars');
  addOption('3', '3 Stars');
  addOption('2', '2 Stars');
  addOption('1', '1 Star');

  // Додавання <select> перед секцією "Features"
  var main = document.querySelector('main');
  var featuresSection = document.getElementById('features');
  main.insertBefore(select, featuresSection);
});